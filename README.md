# Awesome User Styles for the Dolphin Emulator

## Dark

- [Kuroi](https://gitlab.com/dolphin-emu/user-styles/Kuroi)
- [qt-dark](https://gitlab.com/dolphin-emu/user-styles/qt-dark)
- [Flat](https://gitlab.com/dolphin-emu/user-styles/flat)

## Light

- [Flat](https://gitlab.com/dolphin-emu/user-styles/flat)
